# Bitbucket Pipelines Pipe: Synopsys

​
This pipe uses Synopsys Detect to scan Bitbucket repositories with Synopsys Application Security tools, which include the scanning functionality of Coverity on Polaris and Black Duck.

## YAML Definition, Invoking Black Duck (SCA)

​
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
​

```yaml
- pipe: synopsys/synopsys-detect:0.1.0
  variables:
    BLACKDUCK_SERVER_URL: "<string>"
    BLACKDUCK_ACCESS_TOKEN: "<string>"
    # TOOLS: "<string>" # Optional
    # FORCE_SUCCESS: "<string>" # Optional.
    # FAIL_ON_SEVERITY: "<string>" # Optional.
    # LOG_LEVEL: "<string>" # Optional.
    # CODE_INSIGHT: "<boolean>" # Optional.
    # ADDITIONAL_ARGS: "<string>" # Optional.
```

​

## Black Duck Variables

| Variable            | Usage                                     |
| --------------      | ----------------------------------------- |
| BLACKDUCK_SERVER_URL (\*)     | The Black Duck instance URL. |
| BLACKDUCK_ACCESS_TOKEN (\*)     | The Black Duck API Token. |
| TOOLS    | Select which tool to scan the project with. Default: `SIGNATURE_SCAN,DETECTOR`. |
| FORCE_SUCCESS    | Continue build despite issues found. Default: `false`. |
| FAIL_ON_SEVERITY  | A comma-separated list of policy violation severities that will fail Detect. Allowed Values: `ALL`, `BLOCKER`, `CRITICAL`, `MAJOR`, `MINOR`, `TRIVIAL`, `UNSPECIFIED`, default: `UNSPECIFIED` |
| LOG_LEVEL               | Turn on extra logging information, allowed Values: `TRACE`, `DEBUG`, `INFO`, `MAJOR`, `WARN`, `ERROR`, `FATAL`, default: `OFF` . Default: `INFO`. |
| CODE_INSIGHT               | Turn on to get a codde insight report. Default: `false`. |
| ADDITIONAL_ARGS          | Additional arguments to be passed to the synopsys detect cli. Default: none. See [Detect Docs](https://blackducksoftware.github.io/synopsys-detect/latest/) for a  full list of possible args. |

_(\*) = required variable._
​

## YAML Definition, Invoking Polaris (SAST)

```yaml
- pipe: synopsys/synopsys-detect:0.1.0
  variables:
    POLARIS_SERVER_URL: "<string>"
    POLARIS_ACCESS_TOKEN: "<string>"
    POLARIS_BUILD_IMAGE: "<string>"
    # POLARIS_ADDITIONAL_ARGS: "<string>" # Optional.
```

​

## Polaris Variables

| Variable            | Usage                                     |
| --------------      | ----------------------------------------- |
| POLARIS_SERVER_URL (\*)     | The Polaris instance URL. |
| POLARIS_ACCESS_TOKEN (\*)     | The Polaris Access Token. |
| POLARIS_BUILD_IMAGE (\*)     | The image used to compile & build the project. |
| POLARIS_ADDITIONAL_ARGS          | Additional arguments to be passed to the polaris cli. Default: none. See help section in your polaris instance for a full list of possible args. |

_(\*) = required variable._
​
​
​
(*) = required variable.
For other optional variables, please see help section in your polaris instance for a full list of possible args.
​

## Details

​
Integrating Synopsys Detect into Bitbucket Pipelines adds application security testing automation in your development processes.  Both Static Application Security Testing, and Software Composition Analysis are available to provide complete security coverage for your applications.
​

## Prerequisites

​

* A licensed version of Synopsys Polaris or Black Duck
* The URL of either the Polaris or Black Duck server
* Either a Polaris or Black Duck API Token
​
​

## Examples

### Basic Black Duck scan example

​
Uses Black Duck to scan a Node.js application and break the build if any vulnerabilities found.
​

```yaml
script:
  - npm install
​
  - npm test
​
  - pipe: synopsys/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
​
  - npm publish
```

### Basic Coverity on Polaris scan example

​
Uses Coverity to scan a Java maven application.

```yaml
script:
  - mvn clean package
​
  - pipe: synopsys/synopsys-detect:0.1.0
    variables:
      POLARIS_SERVER_URL: $POLARIS_SERVER_URL
      POLARIS_ACCESS_TOKEN: $POLARIS_ACCESS_TOKEN
      POLARIS_LANGUAGE: "$POLARIS_LANGUAGE"
      POALRIS_LANGUAGE_VERSION: "$POLARIS_LANGUAGE_VERSION"
```

## Advanced example

### Fail on high severity and print pdf report

```yaml
script:
  - npm clean-install

  - npm test

  - pipe: synopsys/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
      FAIL_ON_SEVERITY: "HIGH"
      ADDITIONAL_ARGS: "--detect.risk.report.pdf=TRUE"

  - docker build -t $IMAGE_NAME .

  - docker push $IMAGE_NAME

```

### Basic Docker image scan example

Uses Detect to scan a Docker image to find any vulnerabilities present.

```yaml
script:
  - docker build -t $IMAGE_NAME .

  - pipe: synopsys/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
      TOOLS: "DOCKER,SIGNATURE_SCAN"
      ADDITIONAL_ARGS: "--detect.docker.image=$IMAGE_NAME"

  - docker push $IMAGE_NAME
```

​

## Support

​
If you’d like help with this pipe, or you have an issue or feature request, please visit us at <https://community.synopsys.com/s/>
​
If you’re reporting an issue, please include:

* The version of the pipe
* Relevant logs and error messages
* Steps to reproduce

## License

​
Copyright (c) 2020 Synopsys, Inc. Apache 2.0 licensed, see LICENSE file.
