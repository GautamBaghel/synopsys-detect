#!/bin/bash

function fileExists() {
    if [ "$( find . -name "$1" | wc -l | sed 's/^ *//' )" == "0" ]; 
    then return 1; 
    else return 0; 
    fi
}

function installPython() {
    apt-get update -y && \
    apt-get install python3-pip idle3 -y && \
    pip3 install --no-cache-dir --upgrade pip && \
    \
    # delete cache and tmp files
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/cache/* && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/* && \
    rm -rf /var/lib/apt/lists/* && \
    \
    # make some useful symlinks that are expected to exist
    cd /usr/bin && \
    ln -s idle3 idle && \
    ln -s pydoc3 pydoc && \
    ln -s python3 python && \
    ln -s python3-config python-config && \
    cd /

    wget https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh && \
    bash Anaconda3-5.0.1-Linux-x86_64.sh -b && \
    rm Anaconda3-5.0.1-Linux-x86_64.sh

    # Set path to conda
    echo PATH=/root/anaconda3/bin:$PATH
}

if fileExists "Pipfile" || fileExists "Pipfile.lock" || fileExists "setup.py" || fileExists "requirements.txt" || fileExists "environment.yml"; 
then installPython; 
fi

FAIL_RUN=true
if [ -n "$BLACKDUCK_SERVER_URL" ];
then 
    FAIL_RUN=false
    BLACKDUCK_ACCESS_TOKEN:${BLACKDUCK_ACCESS_TOKEN:?'BLACKDUCK_ACCESS_TOKEN variable missing.'}
    TOOLS=${TOOLS:="SIGNATURE_SCAN,DETECTOR"}
    FORCE_SUCCESS=${FORCE_SUCCESS:="false"}
    FAIL_ON_SEVERITY=${FAIL_ON_SEVERITY:="UNSPECIFIED"}
    LOG_LEVEL=${LOG_LEVEL:="INFO"}
    CODE_INSIGHT=${CODE_INSIGHT:="false"}
    ADDITIONAL_ARGS=${ADDITIONAL_ARGS:=""}
    bash <(curl -s https://detect.synopsys.com/detect.sh) \
        --blackduck.url="$BLACKDUCK_SERVER_URL" \
        --blackduck.api.token="$BLACKDUCK_ACCESS_TOKEN" \
        --detect.tools="$TOOLS"
        --detect.force.success="$FORCE_SUCCESS" \
        --detect.policy.check.fail.on.severities="$FAIL_ON_SEVERITY" \
        --logging.level.com.synopsys.integration="$LOG_LEVEL" \
        "$ADDITIONAL_ARGS"

    if [ "$CODE_INSIGHT" = true ];
    then echo "Run Code insight here!"
    fi
else
    echo "Black Duck URL not found so won't run detect scan."
fi

if [ -n "$POLARIS_SERVER_URL" ]; 
then 
    FAIL_RUN=false
    # required parameters
    POLARIS_BUILD_IMAGE:${POLARIS_BUILD_IMAGE:?'POLARIS_BUILD_IMAGE variable missing.'}
    mkdir /root/.synopsys
    POLARIS_HOME="/root/.synopsys"
    POLARIS_DIST=${POLARIS_DIST:="polaris_cli-linux64.zip"}
    wget "$POLARIS_SERVER_URL"/api/tools/"$POLARIS_DIST"
    DIR=$(zipinfo -1 ${POLARIS_DIST} | grep -oE '^[^/]+' | uniq)
    unzip ${POLARIS_DIST}
    rm "$POLARIS_SERVER_URL"/api/tools/"$POLARIS_DIST"
    cd ${DIR}/bin
    POLARIS_ACCESS_TOKEN=${POLARIS_ACCESS_TOKEN:?'POLARIS_ACCESS_TOKEN variable missing.'}
    polaris analyze -w "$POLARIS_ADDITIONAL_ARGS"
else
    echo "Polaris URL not found so won't run polaris scan."
fi

if [ "$FAIL_RUN" = true ];
then exit 1
fi